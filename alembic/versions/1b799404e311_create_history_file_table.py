"""create history file table

Revision ID: 1b799404e311
Revises: 9d2c74c4ce70
Create Date: 2019-05-19 12:41:20.409743

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "1b799404e311"
down_revision = "9d2c74c4ce70"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "historyfile",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("path", sa.String(), nullable=False),
        sa.Column("last_sync_datetime", sa.DateTime(), nullable=True),
        sa.Column("history_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(["history_id"], ["history.id"]),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("history_id", "path"),
    )
    with op.batch_alter_table("historyfile", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_historyfile_history_id"), ["history_id"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_historyfile_last_sync_datetime"),
            ["last_sync_datetime"],
            unique=False,
        )
        batch_op.create_index(batch_op.f("ix_historyfile_path"), ["path"], unique=False)


def downgrade():
    with op.batch_alter_table("historyfile", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_historyfile_path"))
        batch_op.drop_index(batch_op.f("ix_historyfile_last_sync_datetime"))
        batch_op.drop_index(batch_op.f("ix_historyfile_history_id"))

    op.drop_table("historyfile")
