"""init

Revision ID: 7c6783b52308
Revises:
Create Date: 2019-05-12 23:51:12.226295

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "7c6783b52308"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "player",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("room", sa.Enum("WINAMAX", name="room"), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    with op.batch_alter_table("player", schema=None) as batch_op:
        batch_op.create_index(batch_op.f("ix_player_name"), ["name"], unique=False)
        batch_op.create_index(batch_op.f("ix_player_room"), ["room"], unique=False)

    op.create_table(
        "tournament",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("key", sa.String(), nullable=True),
        sa.Column("room", sa.Enum("WINAMAX", name="room"), nullable=False),
        sa.Column("name", sa.String(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("room", "key"),
    )
    with op.batch_alter_table("tournament", schema=None) as batch_op:
        batch_op.create_index(batch_op.f("ix_tournament_name"), ["name"], unique=False)
        batch_op.create_index(batch_op.f("ix_tournament_room"), ["room"], unique=False)

    op.create_table(
        "table",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("room", sa.Enum("WINAMAX", name="room"), nullable=False),
        sa.Column("name", sa.String(), nullable=True),
        sa.Column("tournament_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(["tournament_id"], ["tournament.id"]),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("room", "name"),
    )
    with op.batch_alter_table("table", schema=None) as batch_op:
        batch_op.create_index(batch_op.f("ix_table_name"), ["name"], unique=False)
        batch_op.create_index(batch_op.f("ix_table_room"), ["room"], unique=False)
        batch_op.create_index(
            batch_op.f("ix_table_tournament_id"), ["tournament_id"], unique=False
        )

    op.create_table(
        "hand",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("key", sa.String(), nullable=True),
        sa.Column("start_datetime", sa.DateTime(), nullable=False),
        sa.Column(
            "variant", sa.Enum("NO_LIMIT_HOLDEM", name="variant"), nullable=False
        ),
        sa.Column(
            "game_type",
            sa.Enum("CASH_GAME", "TOURNAMENT", name="gametype"),
            nullable=False,
        ),
        sa.Column("buy_in", sa.Float(), nullable=True),
        sa.Column("level", sa.Integer(), nullable=True),
        sa.Column("ante", sa.Float(), nullable=True),
        sa.Column("small_blind", sa.Float(), nullable=False),
        sa.Column("big_blind", sa.Float(), nullable=False),
        sa.Column("max_seats", sa.Integer(), nullable=False),
        sa.Column("table_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(["table_id"], ["table.id"]),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("table_id", "key"),
    )
    with op.batch_alter_table("hand", schema=None) as batch_op:
        batch_op.create_index(batch_op.f("ix_hand_buy_in"), ["buy_in"], unique=False)
        batch_op.create_index(
            batch_op.f("ix_hand_game_type"), ["game_type"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_hand_start_datetime"), ["start_datetime"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_hand_table_id"), ["table_id"], unique=False
        )
        batch_op.create_index(batch_op.f("ix_hand_variant"), ["variant"], unique=False)

    op.create_table(
        "board",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("flop_1", sa.String(), nullable=True),
        sa.Column("flop_2", sa.String(), nullable=True),
        sa.Column("flop_3", sa.String(), nullable=True),
        sa.Column("turn", sa.String(), nullable=True),
        sa.Column("river", sa.String(), nullable=True),
        sa.Column("hand_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(["hand_id"], ["hand.id"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("id"),
    )
    with op.batch_alter_table("board", schema=None) as batch_op:
        batch_op.create_index(batch_op.f("ix_board_hand_id"), ["hand_id"], unique=True)

    op.create_table(
        "round",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column(
            "type",
            sa.Enum(
                "BLINDS",
                "PREFLOP",
                "FLOP",
                "TURN",
                "RIVER",
                "SHOWDOWN",
                name="roundtype",
            ),
            nullable=False,
        ),
        sa.Column("order", sa.Integer(), nullable=False),
        sa.Column("hand_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(["hand_id"], ["hand.id"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("id"),
    )
    with op.batch_alter_table("round", schema=None) as batch_op:
        batch_op.create_index(batch_op.f("ix_round_hand_id"), ["hand_id"], unique=False)
        batch_op.create_index(batch_op.f("ix_round_order"), ["order"], unique=False)
        batch_op.create_index(batch_op.f("ix_round_type"), ["type"], unique=False)

    op.create_table(
        "seat",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("number", sa.Integer(), nullable=False),
        sa.Column("stack", sa.Float(), nullable=False),
        sa.Column(
            "position",
            sa.Enum(
                "SMALL_BLIND",
                "BIG_BLIND",
                "BUTTON",
                "CUTOFF",
                "UTG_1",
                "UTG_2",
                "UTG_3",
                "MP_1",
                "MP_2",
                "MP_3",
                name="seatposition",
            ),
            nullable=False,
        ),
        sa.Column("order", sa.Integer(), nullable=False),
        sa.Column("is_hero", sa.Boolean(), nullable=False),
        sa.Column("card_1", sa.String(), nullable=True),
        sa.Column("card_2", sa.String(), nullable=True),
        sa.Column("hand_id", sa.Integer(), nullable=False),
        sa.Column("player_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(["hand_id"], ["hand.id"], ondelete="CASCADE"),
        sa.ForeignKeyConstraint(["player_id"], ["player.id"]),
        sa.PrimaryKeyConstraint("id"),
    )
    with op.batch_alter_table("seat", schema=None) as batch_op:
        batch_op.create_index(batch_op.f("ix_seat_card_1"), ["card_1"], unique=False)
        batch_op.create_index(batch_op.f("ix_seat_card_2"), ["card_2"], unique=False)
        batch_op.create_index(batch_op.f("ix_seat_hand_id"), ["hand_id"], unique=False)
        batch_op.create_index(batch_op.f("ix_seat_number"), ["number"], unique=False)
        batch_op.create_index(batch_op.f("ix_seat_order"), ["order"], unique=False)
        batch_op.create_index(
            batch_op.f("ix_seat_player_id"), ["player_id"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_seat_position"), ["position"], unique=False
        )

    op.create_table(
        "action",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("order", sa.Integer(), nullable=False),
        sa.Column(
            "type",
            sa.Enum(
                "ANTE",
                "SMALL_BLIND",
                "BIG_BLIND",
                "CHECK",
                "CALL",
                "BET",
                "RAISE",
                "FOLD",
                "SHOW",
                "COLLECT",
                name="actiontype",
            ),
            nullable=False,
        ),
        sa.Column("value", sa.Float(), nullable=True),
        sa.Column("all_in", sa.Boolean(), nullable=False),
        sa.Column("round_id", sa.Integer(), nullable=False),
        sa.Column("seat_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(["round_id"], ["round.id"], ondelete="CASCADE"),
        sa.ForeignKeyConstraint(["seat_id"], ["seat.id"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("round_id", "order"),
    )
    with op.batch_alter_table("action", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_action_round_id"), ["round_id"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_action_seat_id"), ["seat_id"], unique=False
        )
        batch_op.create_index(batch_op.f("ix_action_type"), ["type"], unique=False)


def downgrade():
    with op.batch_alter_table("action", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_action_type"))
        batch_op.drop_index(batch_op.f("ix_action_seat_id"))
        batch_op.drop_index(batch_op.f("ix_action_round_id"))

    op.drop_table("action")
    with op.batch_alter_table("seat", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_seat_position"))
        batch_op.drop_index(batch_op.f("ix_seat_player_id"))
        batch_op.drop_index(batch_op.f("ix_seat_order"))
        batch_op.drop_index(batch_op.f("ix_seat_number"))
        batch_op.drop_index(batch_op.f("ix_seat_hand_id"))
        batch_op.drop_index(batch_op.f("ix_seat_card_2"))
        batch_op.drop_index(batch_op.f("ix_seat_card_1"))

    op.drop_table("seat")
    with op.batch_alter_table("round", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_round_type"))
        batch_op.drop_index(batch_op.f("ix_round_order"))
        batch_op.drop_index(batch_op.f("ix_round_hand_id"))

    op.drop_table("round")
    with op.batch_alter_table("board", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_board_hand_id"))

    op.drop_table("board")
    with op.batch_alter_table("hand", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_hand_variant"))
        batch_op.drop_index(batch_op.f("ix_hand_table_id"))
        batch_op.drop_index(batch_op.f("ix_hand_start_datetime"))
        batch_op.drop_index(batch_op.f("ix_hand_game_type"))
        batch_op.drop_index(batch_op.f("ix_hand_buy_in"))

    op.drop_table("hand")
    with op.batch_alter_table("table", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_table_tournament_id"))
        batch_op.drop_index(batch_op.f("ix_table_room"))
        batch_op.drop_index(batch_op.f("ix_table_name"))

    op.drop_table("table")
    with op.batch_alter_table("tournament", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_tournament_room"))
        batch_op.drop_index(batch_op.f("ix_tournament_name"))

    op.drop_table("tournament")
    with op.batch_alter_table("player", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_player_room"))
        batch_op.drop_index(batch_op.f("ix_player_name"))

    op.drop_table("player")
