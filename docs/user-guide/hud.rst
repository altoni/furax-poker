.. _user-guide-hud:

===
HUD
===

Synchronization
===============

Once you have configured your hand history folder in the
:ref:`user-guide-settings`, Furax Poker will start synchronizing your history
and monitoring file changes.  The progress of synchronization process is
displayed in the status bar, at the bottom right of main window.

.. image:: synchronizing.png

Please note that is process might be (very) long, but the HUD is usable during
synchronization. Moreover, you can quit the application at any time to pause
the synchronization, it will just restart where it has stopped.

Monitoring
==========

Every new poker table detected by the application will be displayed in the main
window where you can control HUD activation.

.. image:: main_window.png

Statistics
==========

HUD windows display statistics on two "pages", just click on the window to go
to next page. You can drag the windows to position them next to referred player.

Each window displays the player name and the number of hands played taken into
account to compute statistics. On first page the statistics are the following:

**Voluntarily put money in pot (VPIP)**
  Percentage of hands in which player limps, calls or raises before the flop.

**Preflop raise (PFR)**
  Percentage of hands in which player makes a preflop raise.

**Aggression factor (AF)**
  Ratio of the amount of time a player is aggressive (he raises or bets) vs.
  the amount of time that he's passive (he just calls).

.. image:: hud_main_view.png
   :alt: HUD first page

On second page, the statistics are:

**Continuation bet (CBet)**
  Percentage of hands the player bets the flop after raising preflop. The
  number of opportunities (taken or missed) is indicated in parenthesis.

**Fold to continuation bet**
  Percentage of hands the player folds after a CBet. The number of
  opportunities (taken or missed) is indicated in parenthesis.

.. image:: hud_secondary_view.png
   :alt: HUD second page

Hover a statistics to display its name if you forgot what a number refers to.
