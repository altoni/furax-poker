import time

from PyQt5.QtCore import QEventLoop
from PyQt5.QtWidgets import QApplication
import pytest

from furax_poker.db import init_db, session_scope
from furax_poker.hand import Room
from furax_poker.settings import ApplicationSettings, History


@pytest.fixture
def app_data_dir(tmpdir):
    return tmpdir


@pytest.fixture
def session(app_data_dir):
    init_db(app_data_dir, "data.db")
    with session_scope() as session:
        yield session


@pytest.fixture(scope="session")
def event_loop():
    app = QApplication.instance()
    if app is None:
        global _qapp_instance
        _qapp_instance = QApplication([])
        return _qapp_instance
    else:
        return app


@pytest.fixture
def detect_changes(event_loop):
    def _detect_changes(sleep=0.3):
        time.sleep(sleep)
        event_loop.processEvents(QEventLoop.AllEvents, 500)

    return _detect_changes


@pytest.fixture
def room():
    return Room.WINAMAX


@pytest.fixture
def history(session, room, tmp_path):
    history = History(path=str(tmp_path), room=room, hero="Hero")
    session.add(history)
    session.commit()
    return history


@pytest.fixture
def settings(session):
    settings = ApplicationSettings()
    session.add(settings)
    session.commit()
    return settings
