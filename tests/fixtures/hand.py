import datetime
from pathlib import Path
from random import random
import time
from typing import cast, List, Optional, Tuple, Union
import uuid

import pytest
from sqlalchemy.orm import Session

from furax_poker.db import get_or_create
from furax_poker.hand import (
    Action,
    ActionType,
    Board,
    GameType,
    Hand,
    Player,
    Room,
    Round,
    RoundType,
    Seat,
    SeatPosition,
    Table,
    Tournament,
    Variant,
)


@pytest.fixture
def hand_builder(session, room):
    return HandBuilder(session, room)


@pytest.fixture
def raw_hand():
    def _raw_hand(name=None):
        with Path(f"./tests/fixtures/hands/{name}.txt").open() as f:
            raw_hand = f.read()
        return raw_hand

    return _raw_hand


SeatingPlan = List[
    Union[
        Tuple[int, str],
        Tuple[int, str, float],
        Tuple[int, str, SeatPosition],
        Tuple[int, str, float, SeatPosition],
    ]
]

ActionList = List[
    Union[
        Tuple[str, ActionType],
        Tuple[str, ActionType, float],
        Tuple[str, ActionType, float, bool],
    ]
]


class HandBuilder:
    """Utility class to create hands easily."""

    def __init__(self, session: Session, room: Room):
        self.session = session
        self.room = room
        self.seating_plan: Optional[SeatingPlan] = None
        self.table: Optional[Table] = None
        self.tournament: Optional[Tournament] = None

    def create(
        self,
        tournament: Tournament = None,
        table: Table = None,
        start_datetime: datetime.datetime = None,
        max_seats: int = None,
        seating_plan: SeatingPlan = None,
        preflop: ActionList = None,
        flop: ActionList = None,
        turn: ActionList = None,
        river: ActionList = None,
        showdown: ActionList = None,
        board: List[str] = None,
    ) -> Hand:
        if seating_plan is None:
            if self.seating_plan is None:
                raise ValueError("A seating plan must be provided!")
            seating_plan = self.seating_plan
        if max_seats is None:
            max_seats = len(seating_plan)
        if board is None:
            board = []
        if table is None:
            if self.table is None:
                table = get_or_create(self.session, Table, room=self.room, name="Table")
            else:
                table = self.table
        if self.tournament is not None:
            tournament = self.tournament
        if tournament is not None:
            table.tournament = tournament
        if start_datetime is None:
            start_datetime = datetime.datetime.now()
        if preflop is None:
            preflop = []
        if flop is None:
            flop = []
        if turn is None:
            turn = []
        if river is None:
            river = []
        if showdown is None:
            showdown = []
        if board is None:
            board = []

        # Create players from seating plan
        # Look for the button seat (required)
        player_by_name = {}
        button_seat = None
        for seat_number, player_name, *extra in seating_plan:
            player = get_or_create(
                self.session, Player, room=self.room, name=player_name
            )
            player_by_name[player_name] = player
            if extra and extra[0] == SeatPosition.BUTTON:
                button_seat = seat_number
            elif len(extra) == 2 and extra[1] == SeatPosition.BUTTON:
                button_seat = seat_number

        if button_seat is None:
            raise ValueError("A button seat must be defined!")

        small_blind = round(random() * 100)
        big_blind = 2 * small_blind

        # Generate random and unique UUID (based on current timestamp)
        time.sleep(0.001)
        key = str(uuid.uuid4())

        hand = Hand(
            key=key,
            start_datetime=start_datetime,
            variant=Variant.NO_LIMIT_HOLDEM,
            game_type=GameType.CASH_GAME,
            small_blind=small_blind,
            big_blind=big_blind,
            max_seats=max_seats,
            table=table,
            board=Board.from_list(board),
        )
        self.session.add(hand)

        # Create seats from seating plan
        seat_by_player_name = {}
        seat_positions = SeatPosition.list_available(len(seating_plan))

        # Order players from small blind to button
        ordered_seating_plan = sorted(
            seating_plan,
            key=lambda s: (s[0] - cast(int, button_seat) - 1) % cast(int, max_seats),
        )
        for i, (seat_number, player_name, *extra) in enumerate(ordered_seating_plan):
            player = player_by_name[player_name]
            if extra and isinstance(extra[0], float):
                stack = extra[0]
            else:
                stack = round(random() * 10000)
            seat = Seat(
                player=player,
                position=seat_positions[i],
                number=seat_number,
                order=seat_positions[i].order,
                stack=stack,
                is_hero=player.name == "Hero",
            )
            seat_by_player_name[player_name] = seat
            hand.seats.append(seat)

        # Create rounds and actions from action list
        small_blind_player = next(
            seat.player
            for seat in hand.seats
            if seat.position == SeatPosition.SMALL_BLIND
        )
        big_blind_player = next(
            seat.player
            for seat in hand.seats
            if seat.position == SeatPosition.BIG_BLIND
        )
        blinds: ActionList = [
            (small_blind_player.name, ActionType.SMALL_BLIND, small_blind),
            (big_blind_player.name, ActionType.BIG_BLIND, big_blind),
        ]
        actions: List[ActionList] = [blinds, preflop, flop, turn, river, showdown]
        for i, round_actions in enumerate(actions):
            round_type = RoundType(i)
            round_ = Round(type=round_type, order=round_type.order)
            hand.rounds.append(round_)
            for j, (player_name, type, *args) in enumerate(round_actions):
                seat = seat_by_player_name[player_name]
                value = args[0] if len(args) > 0 else None
                all_in = cast(bool, args[1]) if len(args) > 1 else False
                round_.actions.append(
                    Action(order=j, type=type, value=value, all_in=all_in, seat=seat)
                )

        return hand
