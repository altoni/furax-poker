from datetime import datetime, timedelta
from unittest.mock import Mock

import pytest

from furax_poker.analyzer import analyze_hand
from furax_poker.hand import ActionType, SeatPosition, Table, Tournament
from furax_poker.hud import Hud
from furax_poker.settings import TimeScope


@pytest.fixture
def tracker():
    return Mock()


@pytest.fixture
def active_window_observer():
    observer = Mock()
    active_window = Mock()
    active_window.title = ""
    observer.active_window = active_window
    return observer


@pytest.fixture
def create_hud(tracker, active_window_observer, settings, hud_window_factory):
    def _create_hud(table):
        return Hud(
            tracker,
            active_window_observer,
            settings,
            table,
            window_factory=hud_window_factory,
        )

    return _create_hud


def test_start_no_hand(room, create_hud, tracker, hud_window_factory):
    table = Table(room=room, name="Table 1")
    hud = create_hud(table)
    hud.start()

    tracker.hand_loaded.connect.assert_called_once()
    assert len(hud_window_factory.windows) == 0


def test_start(session, create_hud, tracker, hud_window_factory, hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[("Hero", ActionType.CALL)],
    )
    analyze_hand(session, hand)
    session.commit()

    hud = create_hud(hand.table)
    hud.start()

    tracker.hand_loaded.connect.assert_called_once()

    # Hand has 5 players
    assert len(hud_window_factory.windows) == 5
    hero_window = hud_window_factory.get("Hero")
    assert hero_window is not None
    assert hero_window.metrics.hand_played == 1


def test_stop(session, create_hud, tracker, hud_window_factory, hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[("Hero", ActionType.CALL)],
    )
    analyze_hand(session, hand)
    session.commit()

    hud = create_hud(hand.table)
    hud.start()

    tracker.hand_loaded.connect.assert_called_once()
    tracker.disconnect.assert_not_called()
    assert len(hud_window_factory.windows) == 5

    hud.stop()

    tracker.hand_loaded.disconnect.assert_called_once_with(hud._on_hand_loaded)
    for window in hud_window_factory.windows:
        assert window.is_closed


def test_update_same_table_new_hand(
    session, create_hud, hud_window_factory, hand_builder
):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[("Hero", ActionType.CALL)],
    )
    analyze_hand(session, hand)
    session.commit()

    hud = create_hud(hand.table)
    hud.start()

    # Hand has 5 players
    assert len(hud_window_factory.windows) == 5
    hero_window = hud_window_factory.get("Hero")
    assert hero_window.metrics.hand_played == 1

    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[("Hero", ActionType.CALL)],
    )
    analyze_hand(session, hand)
    session.commit()
    hud._on_hand_loaded(hand)

    assert len(hud_window_factory.windows) == 5

    # Check that windows were updated but players are still from last hand
    assert hero_window.metrics.hand_played == 2


def test_update_same_table_player_change(
    session, create_hud, hud_window_factory, hand_builder
):
    t0 = datetime.now()
    t1 = t0 + timedelta(minutes=5)
    hand = hand_builder.create(
        start_datetime=t0,
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
    )
    session.commit()

    hud = create_hud(hand.table)
    hud.start()

    # Hand has 5 players
    assert len(hud_window_factory.windows) == 5
    player_3_window = hud_window_factory.get("Player 3")
    assert player_3_window is not None

    new_hand = hand_builder.create(
        start_datetime=t1,
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "New player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
    )
    session.commit()
    hud._on_hand_loaded(new_hand)

    assert len(hud_window_factory.windows) == 5

    # Check that same window was reused
    assert player_3_window.seat.player.name == "New player 3"


def test_update_same_table_old_hand(
    session, create_hud, hud_window_factory, hand_builder
):
    t0 = datetime.now()
    t1 = t0 - timedelta(minutes=5)
    hand = hand_builder.create(
        start_datetime=t0,
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[("Hero", ActionType.CALL)],
    )
    analyze_hand(session, hand)
    session.commit()

    hud = create_hud(hand.table)
    hud.start()

    # Hand has 5 players
    assert len(hud_window_factory.windows) == 5
    player_3_window = hud_window_factory.get("Player 3")
    assert player_3_window is not None
    hero_window = hud_window_factory.get("Hero")
    assert hero_window is not None
    assert hero_window.metrics.hand_played == 1

    new_hand = hand_builder.create(
        start_datetime=t1,
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "New player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[("Hero", ActionType.CALL)],
    )
    analyze_hand(session, new_hand)
    session.commit()
    hud._on_hand_loaded(new_hand)

    assert len(hud_window_factory.windows) == 5

    # Check that windows were updated but players are still from last hand
    assert player_3_window.seat.player.name == "Player 3"
    assert hero_window.metrics.hand_played == 2


def test_update_new_table_player_change_tournament(
    session, room, create_hud, hud_window_factory, hand_builder,
):
    hand_builder.tournament = Tournament(room=room, name="Tournament")
    table_1 = Table(room=room, name="Table 1")
    table_2 = Table(room=room, name="Table 2")
    hand_builder.create(
        table=table_1,
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
    )
    session.commit()

    hud = create_hud(table_1)
    hud.start()

    player_3_window = hud_window_factory.get("Player 3")
    assert player_3_window is not None

    new_hand = hand_builder.create(
        table=table_2,
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "New player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
    )
    session.commit()
    hud._on_hand_loaded(new_hand)

    assert len(hud_window_factory.windows) == 5

    # Check that same window was reused
    assert player_3_window.seat.player.name == "New player 3"


@pytest.mark.parametrize("is_playing_tournament", [False, True])
@pytest.mark.parametrize(
    "opponent_time_scope", [TimeScope.ALL_TIME, TimeScope.CURRENT_SESSION]
)
@pytest.mark.parametrize(
    "hero_time_scope", [TimeScope.ALL_TIME, TimeScope.CURRENT_SESSION]
)
def test_hero_time_scope(
    session,
    settings,
    room,
    is_playing_tournament,
    hero_time_scope,
    opponent_time_scope,
    create_hud,
    hud_window_factory,
    hand_builder,
):
    """Test that user can restrict HUD time scope for hero and/or opponent.

    Statistics can be displayed for either all time hands or only those of current
    session. Current session is defined as current table for cash game and current
    tournament for MTT.
    """
    settings.hero_time_scope = hero_time_scope
    settings.opponent_time_scope = opponent_time_scope
    old_table_1 = Table(room=room, name="Old table 1")
    hand_1 = hand_builder.create(
        table=old_table_1,
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[("Hero", ActionType.CALL)],
    )
    analyze_hand(session, hand_1)
    session.commit()

    old_table_2 = Table(room=room, name="Old table 2")
    hand_2 = hand_builder.create(
        table=old_table_2,
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[("Player 4", ActionType.CALL)],
    )
    analyze_hand(session, hand_2)
    session.commit()

    if is_playing_tournament:
        hand_builder.tournament = Tournament(room=room, name="Tournament")
        first_tournament_table = Table(room=room, name="First tournament table")
        hand = hand_builder.create(
            table=first_tournament_table,
            seating_plan=[
                (1, "Player 1"),
                (2, "Player 2"),
                (3, "Player 3"),
                (4, "Player 4"),
                (5, "Hero", SeatPosition.BUTTON),
            ],
            preflop=[("Hero", ActionType.CALL), ("Player 4", ActionType.CALL)],
        )
        analyze_hand(session, hand)
        session.commit()

    current_table = Table(room=room, name="Current table")
    hand = hand_builder.create(
        table=current_table,
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[("Hero", ActionType.CALL), ("Player 4", ActionType.CALL)],
    )
    analyze_hand(session, hand)
    session.commit()

    hud = create_hud(hand.table)
    hud.start()

    hero_window = hud_window_factory.get("Hero")
    expected_hero_hand_played = 3 if is_playing_tournament else 2
    if hero_time_scope == TimeScope.CURRENT_SESSION:
        expected_hero_hand_played -= 1
    assert hero_window.metrics.hand_played == expected_hero_hand_played

    opponent_window = hud_window_factory.get("Player 4")
    expected_opponent_hand_played = 3 if is_playing_tournament else 2
    if opponent_time_scope == TimeScope.CURRENT_SESSION:
        expected_opponent_hand_played -= 1
    assert opponent_window.metrics.hand_played == expected_opponent_hand_played


def test_close_unused_window(session, create_hud, hud_window_factory, hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
    )
    session.commit()

    hud = create_hud(hand.table)
    hud.start()

    # Hand has 5 players
    assert len(hud_window_factory.windows) == 5
    player_4_window = hud_window_factory.get("Player 4")
    assert not player_4_window.is_closed

    new_hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
    )
    session.commit()
    hud._on_hand_loaded(new_hand)

    # Hand has now 4 players
    assert len(hud_window_factory.windows) == 4
    assert player_4_window.is_closed


def test_clear_closed_window_from_cache_to_prevent_reopen(
    session, create_hud, hud_window_factory, hand_builder
):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
    )
    session.commit()

    hud = create_hud(hand.table)
    hud.start()

    player_4_window = hud_window_factory.get("Player 4")
    assert not player_4_window.is_closed

    new_hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
    )
    session.commit()
    hud._on_hand_loaded(new_hand)

    assert player_4_window.is_closed

    hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
    )
    session.commit()
    hud._on_hand_loaded(new_hand)

    assert player_4_window.is_closed


def test_create_new_window_if_new_player(
    session, create_hud, hud_window_factory, hand_builder
):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
    )
    session.commit()

    hud = create_hud(hand.table)
    hud.start()

    # Hand has 4 players
    assert len(hud_window_factory.windows) == 4
    with pytest.raises(StopIteration):
        hud_window_factory.get("Player 4")

    new_hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
    )
    session.commit()
    hud._on_hand_loaded(new_hand)

    # Hand has now 5 players
    assert len(hud_window_factory.windows) == 5
    player_4_window = hud_window_factory.get("Player 4")
    assert player_4_window is not None


def test_settings_changes(
    session, room, settings, create_hud, hud_window_factory, hand_builder,
):
    hand_1 = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[("Hero", ActionType.CALL)],
    )
    analyze_hand(session, hand_1)
    session.commit()
    current_table = Table(room=room, name="Current table")
    hand_2 = hand_builder.create(
        table=current_table,
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[("Hero", ActionType.CALL)],
    )
    analyze_hand(session, hand_2)
    session.commit()

    hud = create_hud(hand_2.table)
    hud.start()

    hero_window = hud_window_factory.get("Hero")
    assert hero_window.metrics.hand_played == 2
    assert hud._position_strategy.hide_hud_when_window_not_active is True

    settings.hide_hud_when_window_not_active = False
    settings.hero_time_scope = TimeScope.CURRENT_SESSION
    hud.settings = settings
    assert hero_window.metrics.hand_played == 1
    assert hud._position_strategy.hide_hud_when_window_not_active is False

    # Test changing settings locally
    hud.hero_time_scope = TimeScope.ALL_TIME
    assert hero_window.metrics.hand_played == 2
