from typing import TYPE_CHECKING

from PyQt5.QtWidgets import QProgressBar, QWidget

from furax_poker.gui.utils import UiAttrs
from furax_poker.utils import resource_path

if TYPE_CHECKING:
    from furax_poker.gui.application import Application


class SyncIndicator(QWidget, UiAttrs):
    _progressBar: QProgressBar
    _loadRatio = 0.7

    def __init__(self, app: "Application"):
        super().__init__()
        self.app = app
        self.loadUi(resource_path("data/ui/sync_indicator.ui"))
        self.hide()

        self._progressBar.setMinimum(0)
        self._progressBar.setMaximum(100)

        self.app.tracker.load_progress.connect(
            lambda progress: self._updateProgress(progress * self._loadRatio)
        )
        self.app.tracker.analyze_progress.connect(
            lambda progress: self._updateProgress(
                progress * (1 - self._loadRatio) + self._loadRatio
            )
        )
        self.app.tracker.sync_finished.connect(self.hide)

    def _updateProgress(self, progress: float):
        self.show()
        self._progressBar.setValue(round(100 * progress))
