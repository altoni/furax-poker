import logging
from typing import Callable, Dict, Optional, Protocol

from sqlalchemy import desc

from furax_poker.active_window import ActiveWindowObserver
from furax_poker.db import session_scope
from furax_poker.fact import FactMetrics
from furax_poker.hand import Hand, Seat, Table
from furax_poker.position import PositionStrategy
from furax_poker.settings import ApplicationSettings, TimeScope
from furax_poker.tracker import Tracker

logger = logging.getLogger(__name__)


class HudWindowProtocol(Protocol):
    seat: Seat
    x: int
    y: int
    is_visible: bool

    def refresh(self, seat: Seat, metrics: FactMetrics):
        ...

    def move(self, x: int, y: int):
        ...

    def show(self):
        ...

    def hide(self):
        ...

    def close(self):
        ...


class Hud:
    """Controller of a HUD for a particular poker table."""

    def __init__(
        self,
        tracker: Tracker,
        active_window_observer: ActiveWindowObserver,
        settings: ApplicationSettings,
        table: Table,
        window_factory: Callable[["Hud"], HudWindowProtocol],
    ):
        self.tracker = tracker
        self.active_window_observer = active_window_observer
        self.table = table
        self.window_factory = window_factory

        self._window_by_seat_number: Dict[int, HudWindowProtocol] = {}
        self._position_strategy = PositionStrategy(
            favorite_seat=settings.favorite_seat,
            hide_hud_when_window_not_active=settings.hide_hud_when_window_not_active,
            follow_active_window=settings.follow_active_window,
        )
        self._settings = settings
        self._hero_time_scope: Optional[TimeScope] = None
        self._opponent_time_scope: Optional[TimeScope] = None

    @property
    def settings(self) -> ApplicationSettings:
        """User global settings"""
        return self._settings

    @settings.setter
    def settings(self, settings: ApplicationSettings):
        """Apply new settings for HUD"""
        self._settings = settings
        self._position_strategy.favorite_seat = settings.favorite_seat
        self._position_strategy.hide_hud_when_window_not_active = (
            settings.hide_hud_when_window_not_active
        )
        self._position_strategy.follow_active_window = settings.follow_active_window
        self._position_strategy.apply(self.active_window_observer.active_window)
        self._on_hand_loaded(None)

    @property
    def hero_time_scope(self) -> TimeScope:
        """Hero HUD time scope local settings"""
        return self._hero_time_scope or self.settings.hero_time_scope

    @hero_time_scope.setter
    def hero_time_scope(self, time_scope: TimeScope):
        """Reload statistics with custom time scope"""
        self._hero_time_scope = time_scope
        self._on_hand_loaded(None)

    @property
    def opponent_time_scope(self) -> TimeScope:
        """Opponent HUD time scope local settings"""
        return self._opponent_time_scope or self.settings.opponent_time_scope

    @opponent_time_scope.setter
    def opponent_time_scope(self, time_scope: TimeScope):
        """Reload statistics with custom time scope"""
        self._opponent_time_scope = time_scope
        self._on_hand_loaded(None)

    def start(self):
        """Create HUD windows and keep them in sync with database."""
        logger.info(f"Starting HUD for {self.table}")
        self.tracker.hand_loaded.connect(self._on_hand_loaded)
        self.active_window_observer.changed.connect(self._position_strategy.apply)
        self._on_hand_loaded(None)

    def stop(self):
        """Close managed HUD windows."""
        logger.info(f"Closing HUD for {self.table}")
        self.tracker.hand_loaded.disconnect(self._on_hand_loaded)
        self.active_window_observer.changed.disconnect(self._position_strategy.apply)
        for window in self._window_by_seat_number.values():
            window.close()

    def _on_hand_loaded(self, hand: Optional[Hand]):
        """Update managed HUD windows with latest metrics."""
        with session_scope() as session:
            if self.table.tournament:
                filter_clause = [Table.tournament_id == self.table.tournament_id]
            else:
                filter_clause = [Table.id == self.table.id]
            last_hand = (
                session.query(Hand)
                .join(Table)
                .filter(*filter_clause)
                .order_by(desc(Hand.start_datetime))
                .first()
            )
            window_by_seat_number = {}
            if last_hand:
                self.table = last_hand.table  # update table if it's a tournament
                self._position_strategy.refresh(last_hand)
                for seat in last_hand.seats:
                    if seat.number in self._window_by_seat_number:
                        window = self._window_by_seat_number.pop(seat.number)
                    else:
                        window = self.window_factory(self)
                        self._position_strategy.attach(window)

                    if seat.is_hero:
                        time_scope = self.hero_time_scope
                    else:
                        time_scope = self.opponent_time_scope
                    window.refresh(
                        seat,
                        FactMetrics.from_seat(session, seat, time_scope=time_scope),
                    )
                    window_by_seat_number[seat.number] = window

            for window in self._window_by_seat_number.values():
                window.close()
                self._position_strategy.detach(window)

            self._position_strategy.apply(self.active_window_observer.active_window)
            self._window_by_seat_number = window_by_seat_number
