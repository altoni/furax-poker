import ctypes
from dataclasses import dataclass
import logging
import platform
import subprocess
from threading import Thread
import time
from typing import Dict, Optional, Tuple

from PyQt5.QtCore import pyqtSignal, QObject

logger = logging.getLogger(__name__)


@dataclass(frozen=True)
class ActiveWindow:
    """Class representing a window in user client system."""

    id: int
    title: Optional[str] = None
    x: Optional[int] = None
    y: Optional[int] = None
    width: Optional[int] = None
    height: Optional[int] = None


class ActiveWindowObserver(Thread, QObject):
    """Thread monitoring active window in user client system."""

    changed = pyqtSignal(object)

    def __init__(self, throttle_time=0):
        super().__init__()
        super(Thread, self).__init__()
        self.should_stop = False
        self.active_window: Optional[ActiveWindow] = None
        self.throttle_time = throttle_time

    def start(self):
        super().start()
        logger.info("ActiveWindowObserver started")

    def run(self):
        while not self.should_stop:
            active_window = get_active_window()
            if active_window is not None and active_window != self.active_window:
                self.active_window = active_window
                self.changed.emit(active_window)
            time.sleep(self.throttle_time)

    def stop(self):
        if self.is_alive():
            self.should_stop = True
            self.join()
            logger.info("ActiveWindowObserver stopped")


def get_active_window() -> Optional[ActiveWindow]:
    """Returns current active window in user system.

    See: https://github.com/sindresorhus/active-win
    """
    _get_active_window = {
        "Linux": get_active_window_linux,
        "Windows": get_active_window_windows,
    }
    system = platform.system()
    if system in _get_active_window:
        try:
            return _get_active_window[system]()
        except Exception:
            logger.exception(f"Error while getting active window on {system} system")
    return None


def get_active_window_linux() -> Optional[ActiveWindow]:
    result = _run(["xprop", "-root", "_NET_ACTIVE_WINDOW"]).stdout.strip().split(" # ")
    active_window_id = result[1] if len(result) > 1 else None
    if active_window_id is None:
        return None
    xwininfo = _parse_stdout(_run(["xwininfo", "-id", active_window_id]).stdout)
    xpropinfo = _parse_stdout(_run(["xprop", "-id", active_window_id]).stdout)
    title = xpropinfo.get(
        "_NET_WM_NAME(UTF8_STRING)", xpropinfo.get("WM_NAME(STRING)", None)
    )
    lookup = {
        "x": "Absolute upper-left X",
        "y": "Absolute upper-left Y",
        "width": "Width",
        "height": "Height",
    }
    bounds = {}
    for dest, src in lookup.items():
        value = xwininfo.get(src, None)
        bounds[dest] = int(value) if value is not None else None
    return ActiveWindow(id=int(active_window_id, 16), title=title, **bounds)


def _run(*args):
    return subprocess.run(*args, encoding="utf-8", capture_output=True)


def _parse_stdout(stdout: str) -> Dict[str, Optional[str]]:
    result = {}
    for line in stdout.split("\n"):
        line = line.strip()
        key, value = None, None
        if "=" in line:
            key, value = _parse_line(line, "=")
        elif ":" in line:
            key, value = _parse_line(line, ":")
        if key:
            result[key] = value or None
    return result


def _parse_line(line: str, separator: str) -> Tuple[str, str]:
    key, *value = line.split(separator)
    return key.strip(), separator.join(value).strip()


def get_active_window_windows() -> Optional[ActiveWindow]:
    user32 = ctypes.windll.user32  # type: ignore
    active_window_id = user32.GetForegroundWindow()
    if active_window_id is None or active_window_id == 0:
        return None

    length = user32.GetWindowTextLengthW(active_window_id)
    unicode_buffer = ctypes.create_unicode_buffer(length + 1)
    user32.GetWindowTextW(active_window_id, unicode_buffer, length + 1)
    title = unicode_buffer.value
    rect = ctypes.wintypes.RECT()  # type: ignore
    user32.GetWindowRect(active_window_id, ctypes.byref(rect))

    # When window is minimizing, coordinates returned by GetWindowRect are negative.
    # At this step window is still not marked as minimized so it can't be detected
    # reliably. It's safer to just skip such cases right now, as a window minimizing
    # will soon not be the active window.
    if rect.left < 0 and rect.top < 0 and rect.right < 0 and rect.bottom < 0:
        return None

    bounds = {
        "x": rect.left,
        "y": rect.top,
        "width": rect.right - rect.left,
        "height": rect.bottom - rect.top,
    }
    return ActiveWindow(id=active_window_id, title=title, **bounds)
