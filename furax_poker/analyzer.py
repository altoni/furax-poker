import logging
from typing import List

from sqlalchemy.orm import Session

from furax_poker.fact import Fact, FactType
from furax_poker.hand import ActionType, Hand, RoundType

logger = logging.getLogger(__name__)


def analyze_hand(session: Session, hand: Hand) -> None:
    """Analyze given hand and load facts in session."""
    analyzers = [f for f in globals().values() if hasattr(f, "analyzer")]
    for analyzer in analyzers:
        facts = analyzer(hand)
        session.add_all(facts)


def analyzer(f):
    """Simple decorator stamping function as a fact analyzer."""
    f.analyzer = f.__name__
    return f


@analyzer
def analyze_action(hand: Hand) -> List[Fact]:
    """Analyze action type facts for each round."""
    seen_seats = set()
    facts: List[Fact] = []
    for round in hand.rounds:
        for action in round.actions:
            action_type_to_fact_type = {
                RoundType.PREFLOP: {
                    ActionType.CHECK: FactType.PREFLOP_CHECK,
                    ActionType.CALL: FactType.PREFLOP_CALL,
                    ActionType.BET: FactType.PREFLOP_BET,
                    ActionType.RAISE: FactType.PREFLOP_RAISE,
                    ActionType.FOLD: FactType.PREFLOP_FOLD,
                },
                RoundType.FLOP: {
                    ActionType.CHECK: FactType.FLOP_CHECK,
                    ActionType.CALL: FactType.FLOP_CALL,
                    ActionType.BET: FactType.FLOP_BET,
                    ActionType.RAISE: FactType.FLOP_RAISE,
                    ActionType.FOLD: FactType.FLOP_FOLD,
                },
                RoundType.TURN: {
                    ActionType.CHECK: FactType.TURN_CHECK,
                    ActionType.CALL: FactType.TURN_CALL,
                    ActionType.BET: FactType.TURN_BET,
                    ActionType.RAISE: FactType.TURN_RAISE,
                    ActionType.FOLD: FactType.TURN_FOLD,
                },
                RoundType.RIVER: {
                    ActionType.CHECK: FactType.RIVER_CHECK,
                    ActionType.CALL: FactType.RIVER_CALL,
                    ActionType.BET: FactType.RIVER_BET,
                    ActionType.RAISE: FactType.RIVER_RAISE,
                    ActionType.FOLD: FactType.RIVER_FOLD,
                },
            }

            try:
                type = action_type_to_fact_type[round.type][action.type]
                facts.append(Fact(type=type, action=action))
            except KeyError:
                pass

            # First time this seat takes an action, create hand played fact
            if action.seat not in seen_seats:
                facts.append(Fact(type=FactType.HAND_PLAYED, action=action))
                seen_seats.add(action.seat)

    return facts


@analyzer
def analyze_vpip(hand: Hand) -> List[Fact]:
    """Analyze VPIP and PFR."""
    facts: List[Fact] = []

    seen_seats = set()
    for action in hand.rounds[1].actions:
        # Count VPIP only once even on 3bet
        if action.seat in seen_seats:
            continue
        seen_seats.add(action.seat)
        if action.type in {ActionType.BET, ActionType.RAISE, ActionType.CALL}:
            facts.append(Fact(type=FactType.VPIP, action=action))
            if action.type is ActionType.RAISE:
                facts.append(Fact(type=FactType.PFR, action=action))
        elif action.type is ActionType.FOLD:
            facts.append(Fact(type=FactType.VPIP_CHANCE, action=action))

    return facts


@analyzer
def analyze_cbet(hand: Hand) -> List[Fact]:
    """Analyze cbet and fold to cbet."""
    # Find last player to raise during preflop round
    last_preflop_raiser = None
    for action in hand.rounds[1].actions:
        if action.type is ActionType.RAISE:
            last_preflop_raiser = action.seat

    # Nothing to analyze if there's no preflop raiser or no flop round
    if last_preflop_raiser is None or len(hand.rounds) <= 2:
        return []

    facts: List[Fact] = []
    first_flop_raiser = None
    monitor_fold_to_cbet = False
    for action in hand.rounds[2].actions:
        # Find first player to bet during flop round
        if action.type is ActionType.BET:
            first_flop_raiser = action.seat

        if action.seat is last_preflop_raiser:
            if monitor_fold_to_cbet:
                # cbet was already analyzed, it means someone has reraise the cbet
                # Fold to cbet was also already analyzed so just break
                break
            elif action.seat is first_flop_raiser:
                # If the first flop raiser is the last preflop raiser, it's a cbet
                # Create fact and mark cbet has done to start monitoring fold to cbet
                facts.append(Fact(type=FactType.CBET, action=action))
                monitor_fold_to_cbet = True
            elif first_flop_raiser is None:
                # Last preflop raiser has not bet on flop round, it's a missed cbet
                # There's no need to analyze fold to cbet so break
                facts.append(Fact(type=FactType.CBET_CHANCE, action=action))
                break
            else:
                # Last preflop raiser is not first flop raiser, someone has donk bet
                # There's no need to analyze fold to cbet so break
                break
        elif monitor_fold_to_cbet:
            if action.type is ActionType.FOLD:
                facts.append(Fact(type=FactType.FOLD_TO_CBET, action=action))
            else:
                facts.append(Fact(type=FactType.FOLD_TO_CBET_CHANCE, action=action))
    return facts
