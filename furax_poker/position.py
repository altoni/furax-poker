import logging
from typing import cast, Dict, List, Optional, Set, Tuple, TYPE_CHECKING

from furax_poker.active_window import ActiveWindow
from furax_poker.hand import Hand, Seat

if TYPE_CHECKING:
    from furax_poker.hud import HudWindowProtocol

logger = logging.getLogger(__name__)


class PositionStrategy:
    """Handle positioning of a set of HUD windows.

    A strategy handle positioniing of all windows that are attached to it. The refresh
    method must be called at each new loaded hand to let strategy recompute positions
    based on seating plan. The apply method should be called to apply new positions on
    attached windows, or when current active window has changed.Positioning algorithm
    relies on the fact that HUD windows keep the same seat number during its lifecycle.

    This strategy handles table changes and favorite seat but works only by reusing
    already positioned windows. This means that it doesn't provide a smart first
    position for each window, except the center of current active window if available.
    Though, it ensures that once a window is positioned, it will reuse the same
    position for this seat, and even follow active window when it is moved or resized.
    """

    def __init__(
        self,
        favorite_seat=True,
        hide_hud_when_window_not_active=True,
        follow_active_window=True,
    ):
        #: Boolean indicating that hero is always at the same position on the screen,
        #: whatever its current seat number.
        self.favorite_seat = favorite_seat

        #: Boolean indicating if HUD windows should be hidden when user activates
        #: another window than the poker room window for HUD table.
        self.hide_hud_when_window_not_active = hide_hud_when_window_not_active

        #: Boolean indicating if HUD windows should follow poker room window when
        #: available.
        self.follow_active_window = follow_active_window

        #: Windows for which this strategy handles positioning.
        self.windows: Set["HudWindowProtocol"] = set()

        # Track last know hand for this HUD to detect new seating plan.
        self._last_hand: Optional[Hand] = None

        # Track last active window.
        self._last_active_window: Optional[ActiveWindow] = None

        # Cache of the last know positions for each seat number.
        self._reference_position_by_seat_number: Dict[
            int, Tuple[int, int, Optional[ActiveWindow]]
        ] = {}

        # Rotation to apply on each seat to get last hand corresponding seat number.
        # This is used only when seating plan has changed, to retrieve for each seat
        # of new hand the previous seat number that should be used as reference
        # position.
        self._seat_number_rotation: int = 0

        # Set of HUD windows considered as "properly" positioned, meaning the
        # strategy has already applied a valid position on them.
        self._positioned_windows: Set["HudWindowProtocol"] = set()

        # Indicates if application can detect user system active window. If this
        # stays false, the strategy will always displays HUD windows as fail-safe.
        self._has_active_window_support = False

        # Indicates if strategy has guessed that application could'nt detect user
        # system active window. This is used exclusively to reset window positions
        # if it turns out strategy guessed wrong.
        self._guess_has_no_active_window_support = False

    def attach(self, window: "HudWindowProtocol"):
        """Attach a window to the strategy."""
        self.windows.add(window)

    def detach(self, window: "HudWindowProtocol"):
        """Detach a window from the strategy."""
        self.windows.discard(window)
        self._positioned_windows.discard(window)

    def refresh(self, new_hand: Hand):
        """Make strategy refresh the seating plan from given hand."""
        # Compute rotation to apply on each new seat to retrieve corresponding seat
        # in previous hand
        self._seat_number_rotation = self._compute_seat_number_rotation(new_hand)
        self._last_hand = new_hand

        # This strategy remembers position of closed windows to be able to reopen a
        # window at the same place if another player joins the table. However cache
        # must be kept only if hero stays at the same place on the same table.
        if self._seat_number_rotation != 0:
            self._reference_position_by_seat_number = {}

    def apply(self, active_window: Optional[ActiveWindow]):
        """Apply position strategy to each controlled window."""
        # Determine if application can detect user system active window
        if active_window is not None:
            self._has_active_window_support = True
            # If strategy guessed that there were no active window support, it means
            # that it positionned windows randomly on the screen as a fail-safe. All
            # windows should now not be considered at positioned in order to prevent
            # caching their current position as reference.
            if self._guess_has_no_active_window_support:
                self._positioned_windows = set()
                self._guess_has_no_active_window_support = False

        self._cache_current_positions()
        self._handle_visibility(active_window)

        # Apply new position for each window, only if they are currently visible.
        # The visibility condition is not only an optimisation, it would be false to
        # considered windows as positioned if they're not visible and could lead to
        # caching undesired positions on next tick.
        for window in self.windows:
            if window.is_visible:
                x, y = self._get_next_position(window, active_window)
                window.move(x, y)
                self._positioned_windows.add(window)

        self._last_active_window = active_window

    def _cache_current_positions(self):
        """Cache current positions if relevant."""
        for window in self.windows:
            # Current position should be cache only if window is visible, but also
            # only when active window is relevant. If user has follow_active_window
            # option turned off, we don't care, if it hasn't active window detection
            # support, we don't care either, in other case the last active window
            # must be the poker room window. If we cache a bad last active window,
            # it might be used to compute next HUD windows positioning!
            if window in self._positioned_windows and window.is_visible:
                is_last_active_window_usable = (
                    self._last_active_window is not None
                    and self._is_current_table(
                        self._last_hand, self._last_active_window
                    )
                )
                if (
                    not self.follow_active_window
                    or not self._has_active_window_support
                    or is_last_active_window_usable
                ):
                    self._reference_position_by_seat_number[window.seat.number] = (
                        window.x,
                        window.y,
                        self._last_active_window,
                    )

    def _handle_visibility(self, active_window: Optional[ActiveWindow]):
        """Toggle window visibility depending on active window and user preferences."""
        # If active window is None, do nothing except if we're not sure we have active
        # window support.
        if active_window is not None:
            show = not self.hide_hud_when_window_not_active or self._is_current_table(
                self._last_hand, active_window
            )
            for window in self.windows:
                if show:
                    window.show()
                else:
                    window.hide()
        elif not self._has_active_window_support:
            # Application makes no difference between not detecting active window
            # changes and not being able to detect active window. In that case we
            # must fail safely and display all HUD windows.
            self._guess_has_no_active_window_support = True
            for window in self.windows:
                window.show()

    def _get_next_position(
        self, window: "HudWindowProtocol", active_window: Optional[ActiveWindow]
    ) -> Tuple[int, int]:
        """Returns coordinates to apply on given window."""
        reference_position = self._get_reference_position(window)
        is_active_window_usable = (
            active_window is not None
            and self._is_current_table(self._last_hand, active_window)
            and self._has_bounds(active_window)
        )

        # Either a reference position exists and it should be used, otherwise a good
        # first position is the center of poker table window, if we don't have it
        # just choose something else.
        if reference_position:
            x, y, reference_active_window = reference_position
            is_reference_active_window_usable = (
                reference_active_window is not None
                and self._has_bounds(reference_active_window)
            )
            if (
                self.follow_active_window
                and is_active_window_usable
                and is_reference_active_window_usable
            ):
                x, y = self._handle_active_window_move(
                    cast(ActiveWindow, active_window),
                    cast(ActiveWindow, reference_active_window),
                    x,
                    y,
                )
        elif is_active_window_usable:
            active_window = cast(ActiveWindow, active_window)
            x = int(cast(int, active_window.x) + cast(int, active_window.width) / 2)
            y = int(cast(int, active_window.y) + cast(int, active_window.height) / 2)
        else:
            # TODO: use screen dimensions would be even smarter but would require Qt
            # as dependency in this module which is not desired for now.
            x, y = 0, 0
        return x, y

    def _is_current_table(
        self, hand: Optional[Hand], active_window: ActiveWindow
    ) -> bool:
        """Check if given active window is the poker room window for hand table."""
        if hand:
            return hand.table.name in (active_window.title or "")
        return False

    def _has_bounds(self, active_window: ActiveWindow) -> bool:
        """Check that active window has bounds that can be used to compute position."""
        x, y = active_window.x, active_window.y
        width, height = active_window.width, active_window.height
        if x is not None and y is not None and width is not None and height is not None:
            return True
        return False

    def _get_reference_position(
        self, window: "HudWindowProtocol"
    ) -> Optional[Tuple[int, int, Optional[ActiveWindow]]]:
        """Returns cached position corresponding to given window."""
        # Apply seating plan changes if needed
        seat_number = window.seat.number
        if self.favorite_seat and self._last_hand:
            seat_number = self._rotate_seat_number(
                seat_number, self._last_hand.max_seats, self._seat_number_rotation
            )
        return self._reference_position_by_seat_number.get(seat_number, None)

    def _compute_seat_number_rotation(self, new_hand: Hand) -> int:
        """Return seat rotation to apply to get last hand corresponding seat number.

        Seat number rotation is how much seats there is between its previous
        position and now. Of course it only has an impact if hero position has
        changed.
        """
        seat_number_rotation = 0
        if self._last_hand:
            try:
                last_hero_seat = self._find_hero_seat(self._last_hand.seats)
                hero_seat = self._find_hero_seat(new_hand.seats)
            except StopIteration:
                pass
            else:
                current_seat_number = hero_seat.number
                while current_seat_number != last_hero_seat.number:
                    seat_number_rotation += 1
                    current_seat_number = self._rotate_seat_number(
                        hero_seat.number, new_hand.max_seats, seat_number_rotation
                    )
        return seat_number_rotation

    def _handle_active_window_move(
        self, current: ActiveWindow, reference: ActiveWindow, x: int, y: int,
    ) -> Tuple[int, int]:
        """Compute position to make HUD follow poker window."""
        current_x, current_y = cast(int, current.x), cast(int, current.y)
        current_height = cast(int, current.height)
        reference_x, reference_y = cast(int, reference.x), cast(int, reference.y)
        reference_height = cast(int, reference.height)

        # Translate
        x = x + current_x - reference_x
        y = y + current_y - reference_y

        # Scale
        try:
            # At least on Winamax, ratio width / height is always preserved, except
            # when the sidebar is toggle, so use only height.
            ratio = current_height / reference_height
        except ZeroDivisionError:
            ratio = 1
        local_x, local_y = x - current_x, y - current_y
        local_x, local_y = round(local_x * ratio), round(local_y * ratio)
        x = current_x + local_x
        y = current_y + local_y

        return x, y

    def _rotate_seat_number(self, seat_number: int, max_seats: int, n: int) -> int:
        _seat_number = seat_number + n
        if _seat_number > max_seats:
            _seat_number -= max_seats
        return _seat_number

    def _find_hero_seat(self, seats: List[Seat]) -> Seat:
        return next(seat for seat in seats if seat.is_hero)
